'use strict';

var app = require('./app');
var mysql = require('mysql');
var url = require("url");
var fs = require('fs');

var request = require('request');

var settings = require('./settings.json');

var networks = [];

var connOptions = settings.dbConnection;

function twoSymNum(num)
{
	if (num < 10) return ('0' + num);
	return num;
}

function formatDate(date)
{
	var dd = twoSymNum(date.getDate());
	var mm = twoSymNum(date.getMonth() + 1);
	var yyyy = date.getFullYear();
	var hours = twoSymNum(date.getHours());
	var min = twoSymNum(date.getMinutes());
	var sec = twoSymNum(date.getSeconds());

	return dd + '.' + mm + '.' + yyyy+' '+hours+':'+min+':'+sec;
}


function loadNetworks()
{
	var conn = mysql.createConnection(connOptions);

	var queryString = "CALL `GetNetworks`;";


	var query = conn.query(queryString, function(err, result)
	{
		//console.log(JSON.stringify(result[0]));
		networks = result[0];
		conn.end();
	});
}

loadNetworks();


app.get('/welcome', function (req, res) 
{
	//console.log('ip-------'+req.session.axxip);
	//console.log('mac------'+req.session.axxmac);
	//console.log('get-welcome: req.sessionID is '+req.sessionID);
	
	var query = require('url').parse(req.url,true).query;
	if (query.lang) req.session.language=query.lang;
	
	if (!req.session.language) req.session.language='ru';
	
	if(query.lang=='en') res.render('welcomeEn.jade',{message:req.session.message});
	else res.render('welcomeRu.jade',{message:req.session.message});
});


app.post('/login', function (req, res) 
{		
	//console.log('post-login: req.sessionID is '+req.sessionID);
	res.redirect('/login?lang='+req.body.language);
});

app.get('/login', function (req, res) 
{		
	//console.log('get-login: req.sessionID is '+req.sessionID);
	var query = require('url').parse(req.url,true).query;
	if (query.lang) req.session.language=query.lang;
	
	if(query.lang=='en') res.render('loginEn.jade',{message:req.session.message});
	else res.render('loginRu.jade',{message:req.session.message});
});



app.post('/processing', function (req, res)
{
	//console.log('post-processing: req.sessionID is '+req.sessionID);
	var view = 'waitEn.jade';
	if (req.session.language=='ru') view = 'waitRu.jade';
	
	res.render(view, (err, html) => {
		res.writeHead(200, {"Content-Type": "text/html; charset=utf-8"});
		
		res.write(html + '\n');
		res.write('<center>');
			
		if (req.session.axxip==undefined) req.session.axxip = req.ip.replace('::ffff:', '');

		if (req.session.axxmac==undefined)
		{
			var arp = require('node-arp');	
			arp.getMAC(req.session.axxip, function(err, mac)
			{
				if (!err) 
				{
					req.session.axxmac=mac;
					//console.log('req.session.axxmac is ' +req.session.axxmac);
				}
				else console.log('error extracting MAC: '+JSON.stringify(err));
			});
		}
		
		req.session.code = Math.round(Math.random()*1000000);
		console.log(formatDate(new Date())+'. '+req.body.mobile_num+'-->'+req.session.code);
		
		
		if (req.body.mobile_num == undefined) 
		{
			res.end('<script>window.location.href="/login"</script>');
			return;
		}
		else 
		{					
			req.session.mobile_num = req.body.mobile_num;	
			req.session.mobile_num = req.session.mobile_num.replace('+','');
			if (req.session.mobile_num.charAt(0)=='9') req.session.mobile_num = '7'+req.session.mobile_num;
			
			var counter = 0;
			var timerId = setInterval(function() 
			{
				res.write('.');
				if (req.session.axxmac!=undefined) 
				{
					clearInterval(timerId);
				
					if (req.body.mobile_num == 'TEST!') 
					{
						req.session.mobile_num = req.body.mobile_num;
						
						res.end('<script>setTimeout(function(){window.location.href="/sms"}, 1000);</script>');
						return;
					}
					else
					{

						var conn = mysql.createConnection(connOptions);
						var queryString = "CALL `IdentifyByPhone`('"+req.session.mobile_num+"','"+req.session.axxmac+"','"+req.session.axxip+"');";

						var query = conn.query(queryString, function(err, result)
						{
							req.session.axxIdentifyId = result[0][0].id; //ответ сервера - идентификатор СМС
							
							request({
								url: 'http://json.gate.iqsms.ru/send/',
								method: "POST",
								json: 
								{
									"login": "z1459933240499", 
									"password": "775010",
									"messages":
									{
										"clientId" : req.session.axxIdentifyId,
										"phone": req.session.mobile_num,
										"text": req.session.code,
										"sender": "GorkiHotels" 
									}
								}
							},
							
							//коллбэк отрабатывающий по факту получения ответа от СМС-API
							function (error, response, body) 
							{						
								var descr;
								if (body.status!='ok') descr= body.description;
								else descr= response.body.messages[0].status+ '; smscId='+response.body.messages[0].smscId;	
								
								if (!error && response.statusCode == 200) 
								{
									if(response.body.messages)
									{
										if (response.body.messages[0].status != 'accepted')
										{
											message:req.session.message = response.body.messages[0].status;
											res.end('<script>window.location.href="/login"</script>');
											conn.end();
											return;
										}
										else
										{	
											message:req.session.message = '';
											res.end('<script>window.location.href="/sms"</script>');
											conn.end();
											return;
										}
									}
									else
									{
										message:req.session.message = 'SMS service server says: '+body.description;
										res.end('<script>window.location.href="/wrong"</script>');
										conn.end();
										return;
									}
									saveResponse(req.session.axxIdentifyId, descr);
								}
								else
								{
									saveResponse(req.session.axxIdentifyId, 'SMS service server is unavailable. Response code: '+response.statusCode);
									message:req.session.message = 'SMS service server is unavailable. Response code: '+response.statusCode;
									res.end('<script>window.location.href="/wrong"</script>');
									conn.end();
									return;
								}
							});
						});
					}
				}
				
				if (counter == 100) 
				{
					clearInterval(timerId);
					console.log(formatDate(new Date())+'. Превышен интервал ожидания MAC-адреса');
					
					if (req.session.language=='en') message:req.session.message = "Can't get client device's MAC address! Please refer to systems administrator for support.";
					else  message:req.session.message = 'Не удается получить МАС адрес подключаемого устройства. Пожалуйста, обратитесь за помощью к системному администратору.';
					//res.redirect('/login');
					res.end('<script>window.location.href="/wrong"</script>');
					return;
				}
				else counter++;
				//console.log(counter);
			}, 300);
		}
	});
});

app.get('/sms', function (req, res)
{

	//console.log('get-sms: req.sessionID is '+req.sessionID);
	var query = require('url').parse(req.url,true).query;
	if (query.lang) req.session.language=query.lang;
	
	if(req.session.language=='en') res.render('smsEn.jade',{message:req.session.message});
	else res.render('smsRu.jade',{message:req.session.message});
	
});

app.post('/sms', function (req, res)
{
	//console.log('post-sms: req.sessionID is '+req.sessionID);
	if (req.session.language=='en') res.render('smsEn.jade',{message:req.session.message});
	else res.render('smsRu.jade',{message:req.session.message});
});

app.post('/wrong', function (req, res)
{
	//console.log('post-wrong: req.sessionID is '+req.sessionID);
	res.render('wrong.jade',{message:req.session.message});
});


app.post('/auth', function (req, res)
{
	//console.log('post-auth: req.sessionID is '+req.sessionID);
	//console.log('req.body.sms_code '+req.body.sms_code);
	//console.log('req.session.code '+req.session.code);
	//console.log('auth happened');
	if (req.body.sms_code!=req.session.code)
	{
		//console.log('wrong code');
		
		commitRejection(req);
		if (req.session.language=='en') 
		{
			//console.log('en');
			message:req.session.message = 'Wrong secret code! Please try once again.';
		}
		else  
		{
			//console.log('not en');
			message:req.session.message = 'Неверный код! Попытайтесь еще раз.';
		}
		res.redirect('/sms');
	}
	
	else if (req.body.cbx_agree!='on') 
	{
		//console.log('unchecked box');
		commitRejection(req);
		if (req.session.language=='en') message:req.session.message = 'Please confirm your consent with Internet usage rules!';
		else  message:req.session.message = 'Пожалуйста, подтвердите свое согласие с правилами доступа к сети Интернет!';
		res.redirect('/sms');
	}
	
	else
	{		
		
		var access = openAccess(req);
		var timerId = setInterval(function() 
		{
			if (access=='ok') 
			{
				//console.log('authorized by SMS');
				clearInterval(timerId);
				req.session.destroy();
				res.redirect(settings.defaultURL);
			}
		}, 50);
	}
});


app.post('/vaucher', function (req, res)
{
	//console.log('post-vaucher: req.sessionID is '+req.sessionID);
	res.redirect('/vaucher?lang='+req.body.language);
});

app.get('/vaucher', function (req, res)
{
	//console.log('get-vaucher: req.sessionID is '+req.sessionID);
	var query = require('url').parse(req.url,true).query;
	if (query.lang) req.session.language=query.lang;
	
	if(req.session.language=='en') res.render('vaucherEn.jade',{message:req.session.message});
	else res.render('vaucherRu.jade',{message:req.session.message});
});

/*==================================================================================================================*/
app.post('/vauth', function (req, res)
{
	//console.log('post-vauth: req.sessionID is '+req.sessionID);
	if (req.session.axxip==undefined) req.session.axxip = req.ip.replace('::ffff:', '');

	
	if (req.session.axxmac==undefined)
	{
		var arp = require('node-arp');	
		arp.getMAC(req.session.axxip, function(err, mac)
		{
			if (!err) req.session.axxmac=mac;
		});	
	}
	
	var counter = 0;
	var timerId = setInterval(function() 
	{
		if (req.session.axxmac!=undefined) 
		{
			clearInterval(timerId);
			
			var conn = mysql.createConnection(connOptions);

			var queryString = "CALL `CheckVaucher`('"+req.body.vau_code+"');";
			var query = conn.query(queryString, function(err, result)
			{
				if (result[0][0].status=='none') 
				{
					message:req.session.message = 'Vaucher code is incorrect. Please type code again.';
					commitRejection(req);
					res.redirect('/vaucher');
				}
				else if (result[0][0].status=='active')
				{
					message:req.session.message = 'This vaucher has been activated already. Please use the fresh one.';
					commitRejection(req);
					res.redirect('/vaucher');
				}
				else
				{
					activateVaucher(req, req.body.vau_code);
					
					var access = openAccess(req);
					var timerId2 = setInterval(function() 
					{
						if (access=='ok') 
						{
							//console.log('authorized by vaucher');
							clearInterval(timerId2);
							req.session.destroy();
							res.redirect(settings.defaultURL);
						}
					}, 50);
				}
				conn.end();
			});
		}
		
		if (counter == 10) 
		{
			clearInterval(timerId);
			if (req.session.language=='en') message:req.session.message = "Can't get client device's MAC address! Please refer to systems administrator for support.";
			else  message:req.session.message = 'Не удается получить МАС адрес подключаемого устройства. Пожалуйста, обратитесь за помощью к системному администратору.';
			res.redirect('/login');
		}
		else counter++;
	}, 50);
});
/*==================================================================================================================*/
app.get('/admin', function (req, res)
{
	if (req.session.sysAccess)
	{
		res.render('admin.jade',{session:req.sessionID, sysRole:req.session.axxSysUserRole});
	}
	else
	{
		res.render('sysAuth.jade',{message:req.session.message});
	}
});

app.post('/sysAuth', function (req, res)
{
	var conn = mysql.createConnection(connOptions);
	
	var queryString = "CALL `IdentifySysUser`('"+req.body.username+"','"+req.body.password+"');";

	var query = conn.query(queryString, function(err, result)
	{
		if (result[0].length>0)
		{
			req.session.sysAccess = true;
			req.session.axxSysUser = result[0][0].username;
			req.session.axxSysUserRole = result[0][0].role;

			req.session.message = '';
			res.redirect('/admin');
		}
		else
		{
			req.session.message = 'user or password not found. please log-in once again.';
			res.redirect('/admin');
		}
		conn.end();
	});
});


app.get('/print', function (req, res)
{
	if (req.session.sysAccess)
	{
		var vau = 'ERROR! Please refer to systems administrator for support.';
		var query = require('url').parse(req.url,true).query;
		if (query.code) vau=query.code;
		
		res.render('print.jade', {vaucher:vau});
	}
	else
	{
		res.render('sysAuth.jade');
	}
});

app.get('/wrong', function (req, res)
{
	//console.log('get-wrong: req.sessionID is '+req.sessionID);
	res.render('wrong.jade',{message:req.session.message, message2:req.session.message2});
});

app.get('*', function (req, res) 
{
	if(req.url=='/favicon.ico') return;
	//console.log('get-*: req.sessionID is '+req.sessionID);
	//res.redirect('/welcome');
	//return;
	
	req.session.axxOriginalURL = req.protocol + '://' + req.get('host') + req.originalUrl;
	
	var auth = 0;
	res.render('getMac.jade', (err, html) => {

		res.writeHead(200, {"Content-Type": "text/html; charset=utf-8"});
		res.write(html);
		res.write('Recognizing device');
		
		if (req.session.axxip==undefined) req.session.axxip = req.ip.replace('::ffff:', '');

		if (req.session.axxmac==undefined)
		{
			var arp = require('node-arp');	
			arp.getMAC(req.session.axxip, function(err, mac)
			{
				if (!err) req.session.axxmac=mac;
				else console.log('error extracting MAC: '+JSON.stringify(err));

			});
		}
		
		
		var counter = 0;
		var timerId = setInterval(function() 
		{
			res.write('.');
			if (req.session.axxmac!=undefined) 
			{
				clearInterval(timerId);
				var conn = mysql.createConnection(connOptions);
				var queryString = "CALL `SeekDevice`('"+req.session.axxmac+"');";
				var query = conn.query(queryString, function(err, result)
				{
					auth = result[0][0].result;
					if (auth == 999999)
					{
						markOldFriend(req.session.axxmac, req.session.axxip);
						var access = openAccess(req);
						
						var timerId2 = setInterval(function() 
						{
							if (access=='ok') 
							{
								clearInterval(timerId2);
								//console.log(new Date()+'  Old friend detected. sessionID is '+req.sessionID);

								var maxCnt = 15;
								var timer1_cnt = 0;
								
								var timer1 = setInterval(function()
								{
									if (timer1_cnt == maxCnt) 
									{	
										clearInterval(timer1);
										res.end('<script>setTimeout(function(){window.location.href="'+req.session.axxOriginalURL+'"}, 500);</script>');
										return;
									}
									else res.write('.');
									timer1_cnt++;
								}, 200);
								
								//req.session.destroy();
								var exec = require('child_process').exec;
								var cmd = "netstat -atn | grep "+req.session.axxip+" | grep :"+settings.port+" | awk '{print $5,\" eth1\"}' | xargs killcx";
								//console.log('==========Trying to kill connection==========================');
								var child = exec(cmd,(error, stdout, stderr) => {
									if (error !== null) 
									{
										//console.log(`exec error: ${error}`);
									}
								});
							}
						}, 500);
							
						
						
					}
					else if (auth > 9) 
					{
						res.end('<script>window.location.href="/vaucher"</script>');
						conn.end();
						return;
					}
					else 
					{
						res.end('<script>window.location.href="/welcome"</script>');
						conn.end();
						return;
					}
					
				});	
			}
			
			if (counter == 200) //200 раз по 300мс = 1минута
			{
				clearInterval(timerId);
				console.log(formatDate(new Date())+'. Превышен интервал ожидания MAC-адреса! '+req.ip);
				
				message:req.session.message = "Не удается получить МАС адрес подключаемого устройства. Пожалуйста, обратитесь за помощью к системному администратору.";
				message2:req.session.message2 = "Can't get client device's MAC address! Please refer to systems administrator for support.";
				res.end('<script>window.location.href="/wrong"</script>');
				return;
			}
			else counter++;
		}, 300);
	});

})

function markOldFriend(mac, ip)
{	
	var conn = mysql.createConnection(connOptions);
	var queryString = "CALL `MarkOldFriend`('"+mac+"','"+ip+"');";

	var query = conn.query(queryString, function(err, result)
	{
		conn.end();
	});
	
}


function saveResponse(identifyId, status)
{
	var conn = mysql.createConnection(connOptions);
	var queryString = "CALL `UpdateIdentifyStatus`('"+identifyId+"','"+status+"');";

	var query = conn.query(queryString, function(err, result)
	{
		conn.end();
	});
}

function openAccess(req)
{
//	console.log('openAccess(): req.session.axxmac is ' +req.session.axxmac);
	//console.log('openAccess(): req.session.ip is ' +req.session.axxip);
	//if (req.session.axxip==undefined) req.session.axxip = req.ip.replace('::ffff:', '');
	
	var ipArr = req.session.axxip.split('.');
	
	var j = 0;
	var decIP = 0;
	
	for(var i=ipArr.length-1; i>=0; i--)
	{
		decIP = decIP + ipArr[i]*Math.pow(256, j);
		j++;
		if (j>5) break;
	}
		
	for (var i=0; i<networks.length; i++)
	{
		if ((networks[i].min<=decIP) && (networks[i].max>=decIP))
		{
			req.session.axxAddress = networks[i].address;
			req.session.axxBitmask = networks[i].bitmask;
			req.session.axxNetmask = networks[i].netmask;
			req.session.axxWildcard = networks[i].wildcard;
			req.session.axxNetwork = networks[i].network;
			req.session.axxHostMin = networks[i].hostMin;
			req.session.axxHostMax = networks[i].hostMax;
			req.session.axxBroadcast = networks[i].broadcast;
			req.session.axxHosts = networks[i].hosts;
		}
	}



	var exec = require('child_process').exec;

	var cmd = 'iptables -t nat -D PREROUTING -s '+req.session.axxAddress+'/'+req.session.axxBitmask+' -p tcp -m tcp --dport 80 -j DNAT --to-destination '+req.session.axxHostMin+':8111';
	cmd += ' && '+'iptables -t nat -A PREROUTING -s '+req.session.axxip+' -p tcp -m tcp --dport 80 -j DNAT --to-destination '+req.session.axxHostMin+':3128';
	cmd += ' && '+'iptables -t nat -A PREROUTING -s '+req.session.axxAddress+'/'+req.session.axxBitmask+' -p tcp -m tcp --dport 80 -j DNAT --to-destination '+req.session.axxHostMin+':8111';
	cmd += ' && '+'iptables -D FORWARD -i eth1 -p tcp -m tcp --dport 443 -j DROP';
	cmd += ' && '+'iptables -A FORWARD -i eth1 -p tcp -m tcp --dport 443 -m mac --mac-source '+req.session.axxmac+' -j ACCEPT';
	cmd += ' && '+'iptables -A FORWARD -i eth1 -p tcp -m tcp --dport 443 -j DROP';
	
	var child = exec(cmd,(error, stdout, stderr) => {
		if (error !== null) 
		{
			console.log('Error while iptables modify is trying');
			console.log(`exec error: ${error}`);
		}
		else
		{
			//console.log('========================================================================');
		}
	});
	
	var conn = mysql.createConnection(connOptions);
	var queryString = "CALL `CommitAuth`('"+req.session.axxmac+"');";

	var query = conn.query(queryString, function(err, result)
	{
		conn.end();
	});
	
	return 'ok';
}

function commitRejection(req)
{
	var conn = mysql.createConnection(connOptions);
	var queryString = "CALL `CommitRejection`('"+req.session.axxmac+"');";

	var query = conn.query(queryString, function(err, result)
	{
		conn.end();
	});
}


function activateVaucher(req, vau_code)
{
	var conn = mysql.createConnection(connOptions);
	var queryString = "CALL `IdentifyByVaucher`('"+vau_code+"','"+req.session.axxmac+"','"+req.session.axxip+"');";

	var query = conn.query(queryString, function(err, result)
	{
		conn.end();
	});	
}