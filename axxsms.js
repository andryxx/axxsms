//'use strict';

var server = require('http').createServer();
var mysql = require('mysql');
var WebSocketServer = require('ws').Server;
var settings = require('./settings.json');

var app = require('./app');
require('./routes');

var port = app.get('port');

var connOptions = settings.dbConnection;

var spAccess = {};
var sessions = {};

server.on('request', app);
server.listen(port, function () 
	{
		console.log(formatDate(new Date())+'  Server restarted. Launch http://'+settings.serverAddress+':'+ port+' to begin') 
	});

webSocketServer = new WebSocketServer({server: server});

/*=======================================================================================================================*/
/*Scheduled functions*/
var CronJob = require('cron').CronJob;


new CronJob('0 0 4 * * *', function() {
  //console.log(formatDate(new Date())+'  Launchig scheduled task');
  clearIpTables();
}, null, true, 'Europe/Moscow');
 
function clearIpTables()
{
	var exec = require('child_process').exec;
	var cmd = 'iptables-restore < /usr/local/iptables/default.ipt';

	var child = exec(cmd,(error, stdout, stderr) => {
		if (error !== null) 
		{
			console.log(formatDate(new Date())+'  Error while iptables modify is trying.');
			console.log(`exec error: ${error}`);
		}
		else 
		{
			console.log(formatDate(new Date())+'  IP Tables cleared successfully.');
		}
	});
}
function forgetDevices()
{	
	var connection = mysql.createConnection(connOptions);
	var queryString = "DELETE FROM `devices`;";
	connection.query(queryString, function(err, result)
	{
		console.log(formatDate(new Date())+'  devices have been forgotten');
		connection.end();
	});	
}

//forgetDevices();
//clearIpTables();

//iptables-restore < /usr/local/iptables/default.ipt

/*=======================================================================================================================*/
function twoSymNum(num)
{
	if (num < 10) return ('0' + num);
	return num;
}

function formatDate(date)
{
	var dd = twoSymNum(date.getDate());
	var mm = twoSymNum(date.getMonth() + 1);
	var yyyy = date.getFullYear();
	var hours = twoSymNum(date.getHours());
	var min = twoSymNum(date.getMinutes());
	var sec = twoSymNum(date.getSeconds());

	return dd + '.' + mm + '.' + yyyy+' '+hours+':'+min+':'+sec;
}

function buildQuery(params)
{
	var result = 'CALL `' + params.type+'`(';
	if(params.args)
	{
		var pCount = 0;
		for (var key in params.args)
		{
			pCount += 1;
			if (params.args[key]==null)
			{
				result += "NULL, ";
			}
			else
			{
			result += "'"+params.args[key]+"', ";
			}
		}
		if (pCount>0) result = result.substr(0, result.length-2);
	}
	result += ');';
	return result;
}


/*--------------------------------------------------------------------------------------------------------------------------------*/



function inScope(numb, scope)
{
	scop = +scope;
	num = Math.pow(2, numb);
	//console.log('scop= '+scop+'; num= '+num+'; result= '+((scop&num)>0));
	if ((scop&num)>0) return true;
	else return false;
}

function createAccessMatrix()
{
	var spTable;
	var roles;
	
	var connection = mysql.createConnection(connOptions);
	var queryString = "CALL `GetSpAccessList` ();";
	connection.query(queryString, function(err, result)
	{
		spTable = result[0];
		connection.end();
	});
	
	var connection2 = mysql.createConnection(connOptions);
	var queryString = "CALL `GetSysRolesList` ();";
	connection2.query(queryString, function(err, result)
	{
		roles = result[0];
		connection2.end();
	});
	
	
	var counter = 0;
	var timerId = setInterval(function() 
	{
		if ((spTable!==undefined) && (roles!==undefined))
		{
			clearInterval(timerId);
			
			spAccess[0] = {};
			for (var j=0; j<spTable.length; j++)
			{
				spAccess[0][spTable[j].sp] = inScope(0, spTable[j].access);
			}			
			
			
			for (var i=0; i<roles.length; i++)
			{
				
				var role = {};
				
				for (var j=0; j<spTable.length; j++)
				{
					role[spTable[j].sp] = inScope(roles[i].id, spTable[j].access);
				}
				
				spAccess[roles[i].id] = role;
			}
		}
		
		if (counter == 100) //предохранитель от зацикливания в случае отсутствия ответа от БД
		{
			console.log('Access matrix hasnt been created. No answer from data base!!!');
			clearInterval(timerId);
		}
		else counter++;
	}, 50);	
}

createAccessMatrix();



function refreshSessions()
{	
	var connection = mysql.createConnection(connOptions);
	var queryString = "CALL `GetSessions` ();";
	connection.query(queryString, function(err, result)
	{
		var tbl = result[0];
	
		var inactiveSessions = [];
		for (var key in sessions)
		{
			var inactive = true;
			for (var i=0; i<tbl.length; i++)
			{
				if (key == tbl[i].session_id)
				{
					inactive = false;
					break;
				}
			}
			if (inactive) inactiveSessions.push(key);
		}		
		
		for (var i=0; i<inactiveSessions.length; i++)
		{
			delete sessions[inactiveSessions[i]];
		}
		
		for (var i=0; i<tbl.length; i++)
		{
			var sessDetails = JSON.parse(tbl[i].data);
			
			var sessObj = {};
			sessObj.username = sessDetails.axxSysUser;
			sessObj.role = sessDetails.axxSysUserRole;
			
			sessions[tbl[i].session_id] = sessObj;
		}
		
		connection.end();
	});	
}

//refreshSessions();


function hasAccess(sp, sessID)
{
	if (spAccess[sessions[sessID].role]==undefined) return false;
	if (spAccess[sessions[sessID].role][sp] == undefined) return false;
	return spAccess[sessions[sessID].role][sp];	
}

/*
var cnt = 3;
var timerId = setInterval(function() 
{
	if (cnt==0) 
	{
		clearInterval(timerId);
		console.log(hasAccess('AddVaucher', 'EwgqfcxVVEKNSx_hsldQK942_uhkWVKb'));
	}
	else
	{
		console.log(cnt);
		cnt--;
	}
}, 250);
*/

/*=======================================================================================================================*/

webSocketServer.on('connection', function(ws) {
	ws.on('message', function(params) 
	{
		params = JSON.parse(params);
		
		if (sessions[params.sessionId] == undefined)
		{
			refreshSessions();
			
			var cnt = 5;
			var timerId = setInterval(function() 
			{
				if (cnt==0) 
				{
					clearInterval(timerId);
					console.log('Session '+params.sessionId+' not found. Access denied');
				}
				else
				{
					if (sessions[params.sessionId] == undefined) cnt--;
					else
					{
						clearInterval(timerId);
						bulletFinis(params);
					}
				}
			}, 50);
		}
		else
		{
			bulletFinis(params);
		}
	});

	ws.on('close', function(){delete ws;});
	
	function bulletFinis(params)
	{
		if (hasAccess(params.type, params.sessionId)) //console.log(params.type+': access granted');
		{
			var connection = mysql.createConnection(connOptions);			
			var queryString = buildQuery(params);
			
			connection.query(queryString, function(err, result)
			{
				if (result==undefined)
				{
					console.log('!!!--No Data returned--!!!--%s--!!!', queryString);
					connection.end();
				}
				else
				{
					var json = '{"'+params.result+'":'+JSON.stringify(result[0]);
					if (params.postprocessor) json += ', "postprocessor": "'+params.postprocessor+'"';
					if (params.initiator) json += ', "initiator": "'+params.initiator+'"';
					json += '}';
					//console.log(json);
					ws.send(JSON.stringify(json));
					connection.end();
				}
			});
		}
		else console.log(formatDate(new Date())+' '+params.type+': access denied ('+params.sessionId+')');
	}
});

