'use strict';

var express = require('express');
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var settings = require('./settings.json');
var favicon = require('express-favicon');


var bodyParser = require('body-parser')
	
var app = module.exports = exports = express();

// Configuration


app.set('port', process.env.PORT || settings.port);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');


// Middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
//app.use(favicon(__dirname + '/public/img/favicon.ico'));

var options = settings.dbConnection;
/*
var sessionStore = new session.MemoryStore();
app.use(session({store: sessionStore, secret: 'muhhhaaahha'}));
*/

var sessionStore = new MySQLStore(options);
app.use(session({
    key: 'sessionCookieName',
    secret: 'axxSmsSessionCookieSecret',
    store: sessionStore,
    resave: true,
    saveUninitialized: false,
	cookie: {
		maxAge: 2*60*60*1000 //вот в этом объекте задается время жизни сессии
	  }
}));

app.use(express.static(__dirname + '/public'));