﻿var socket = new WebSocket('ws://'+document.location.host);

socket.onmessage = function(event) // прием сообщений с сервера через установленное соединение WebSocket
{	
	var json = eval(event.data); // избавление от экранировани¤ кавычек (\"), ломающего JSON
	var data = JSON.parse(json);

	if (data.dataSet)
	{
		if(data.initiator) eval(data.postprocessor).call(null, data.dataSet, data.initiator);
		else eval(data.postprocessor).call(null, data.dataSet);
	}
	else if (data.row)
	{
		obj = new Object();
		for (var key in data.row[0]) obj[key] = data.row[0][key];
		eval(data.postprocessor).call(null, obj);
	}
	else if (data.single)
	{
		var single;
		//console.log(single);
		for (var key in data.single[0]) single = data.single[0][key];
		eval(data.postprocessor).call(null, single);
	}
}


socket.onerror = function(error) {
  alert("Ошибка " + error.message);
};

serverTransmission = function()	// псевдокласс, создающий объект, отвечающий за передачу данных (запросов) к серверу
{
	this.clear = function()
	{
		this.sp = '';
		this.result = 'dataSet';
		this.pp = 'noFunc';
		this.args = {};
		if (this.initiator) delete this.initiator;
	}
	
	this.send = function()
	{
		var params =
		{
			type: this.sp,
			sessionId: localStorage.getItem('axxSessionID'),
			result: this.result,
			postprocessor: this.pp,
			initiator: this.initiator,
			args: this.args
		}
		
		var timerId = setInterval(function() {
			if (socket.readyState == 1)
			{
				socket.send(JSON.stringify(params));
				clearInterval(timerId);
			}
			
		}, 100);
		
	}
	
	this.clear();
}

var bullet = new serverTransmission();




//================================================================
// УНИВЕРСАЛЬНЫЕ ФУНКЦИИ

function askCredentials(userName)
{
	bullet.clear();
	bullet.result = 'credentials';
	bullet.sp = userName;
	
	bullet.send();
}

function log(msg){myDIV.innerHTML += msg+"<br/>";}

function noFunc(){}

function dateForInput(str) 	//возврат даты в формате гггг-мм-дд
{
	var date = new Date(str);
	var dd = twoSymNum(date.getDate());
	var mm = twoSymNum(date.getMonth() + 1);
	var yyyy = date.getFullYear();

	return yyyy + '-' + mm + '-' + dd;
}

// УНИВЕРСАЛЬНЫЕ ФУНКЦИИ
//================================================================
// МОДАЛЬНЫЕ ОКНА





//инициализация класса модального окна
function ModalForm(fmName){

	this.caption = fmName;
	this.id = fmName;
	this.inputs = [];
	this.buttons = [];

	this.addTextField = function(inputID, inputLabel, defaulValue)
	{
		this.inputs.push({
			type: 'text',
			id: inputID,
			label: inputLabel,
			value: defaulValue
		});
	}
	
	this.addTextArea = function(inputID, inputLabel, defaulValue)
	{
		this.inputs.push({
			type: 'textarea',
			id: inputID,
			label: inputLabel,
			value: defaulValue
		});
	}

	this.addHiddenField = function(inputID, inputLabel, defaulValue)
	{
		this.inputs.push({
			type: 'hidden',
			id: inputID,
			label: inputLabel,
			value: defaulValue
		});
	}

	this.addPasswordField = function(inputID, inputLabel, defaulValue)
	{
		this.inputs.push({
			type: 'password',
			id: inputID,
			label: inputLabel,
			value: defaulValue
		});

		this.inputs.push({
			type: 'password',
			id: 'rep-'+inputID,
			label: 'Repeat ' + inputLabel,
			value: defaulValue
		});
	}

	this.addDateField = function(inputID, inputLabel, defaulValue)
	{
		this.inputs.push({
			type: 'date',
			id: inputID,
			label: inputLabel,
			value: defaulValue
		});
	}

	this.addComboBox = function(inputID, inputLabel, dataSource, funcEvent)
	{
		var cbox =
		{
			type: 'select',
			id: inputID,
			label: inputLabel,
			event: funcEvent
		};

		if (dataSource)
		{
			cbox.sp = dataSource.storedProcedure;
			cbox.pp = dataSource.postporcessor;
			cbox.args = dataSource.arguments;
		};

		this.inputs.push(cbox);
	}

	this.addMultiSelect = function(inputID, inputLabel, dataSource, funcEvent)
	{
		var cbox =
		{
			type: 'select',
			id: inputID,
			label: inputLabel,
			mult: true,
			event: funcEvent
		};

		if (dataSource)
		{
			cbox.sp = dataSource.storedProcedure;
			cbox.pp = dataSource.postporcessor;
			cbox.args = dataSource.arguments;
		};

		this.inputs.push(cbox);
	}

	this.addCheckBox = function(inputID, inputLabel, defaulValue)
	{
		this.inputs.push({
			type: 'checkbox',
			id: inputID,
			label: inputLabel,
			value: defaulValue
		});
	}


	this.addCloseButton = function(buttonText, func)
	{
		this.buttons.push({
			type: 'close',
			value: buttonText,
			event: func
		});
	}

	this.addButton = function(buttonText, func)
	{
		this.buttons.push({
			type: 'normal',
			value: buttonText,
			event: func
		});
	}

	this.show = function()
	{
		createModal(this);
		showModal(fmName);
	}
}

//конструирование модального окна по заданным параметрам
function createModal(options){

	if (!document.body.shade){
		var div = document.createElement('div');
		div.id="shade";
		document.body.appendChild(div);
	}

	var div = document.createElement('div');
	div.id=options.id;
	div.className = 'modal';

	var caption = document.createElement('h2');
	caption.innerHTML =options.caption;
	div.appendChild(caption);

	if (options.inputs)
	{
		var table = document.createElement('table');
		var tbody = document.createElement('tbody');

		for (var i = 0; i < options.inputs.length; i++)
		{
			var row = document.createElement('tr');
			var fieldColumn = document.createElement('td');
			var labelColumn = document.createElement('td');

			if (options.inputs[i].type == 'select'){
				//создание скрытого поля для хранения значения ИД выпадающего списка - для выбора значения по умолчанию
				var hiddenID = document.createElement('input');
				hiddenID.type = 'hidden';
				hiddenID.id =  options.inputs[i].id+'ID';

				div.appendChild(hiddenID);

				var select = document.createElement('select');
				if(options.inputs[i].mult) select.multiple="multiple";
				select.id =  options.inputs[i].id;
				if (options.inputs[i].event) select.addEventListener('change', options.inputs[i].event);
				fieldColumn.appendChild(select);
			} 
			else if (options.inputs[i].type == 'textarea')
			{
				var input = document.createElement('textarea');
				input.id =  options.inputs[i].id;
				if (options.inputs[i].value) input.innerHTML = options.inputs[i].value;
				fieldColumn.appendChild(input);
			}
			else 
			{
				var input = document.createElement('input');
				input.type = options.inputs[i].type;
				input.id =  options.inputs[i].id;
				if (options.inputs[i].value)
				{
					if (input.type == 'date') {input.value = dateForInput(options.inputs[i].value);}
					else {input.value = options.inputs[i].value;}
				}
				fieldColumn.appendChild(input);

			}

			if (options.inputs[i].type == 'password')	//для полей типа password навешивается обработчик событий, который проверяет соответствие пароля и его повтора
			{

				if (input.id.substr(0,4)=='rep-')
				{
					input.addEventListener('keyup', function(){

						var input2 = document.getElementById(this.id.substring(4));
						if (this.value == input2.value)
						{
							this.classList.remove('redBg');
							this.classList.add('greenBg');
						}
						else
						{
							this.classList.remove('greenBg');
							this.classList.add('redBg');
						}
					});
				}
				else
				{
					input.addEventListener('keyup', function()
					{
						var input2 = document.getElementById('rep-'+this.id);
						if (this.value == input2.value)
						{
							input2.classList.remove('redBg');
							input2.classList.add('greenBg');
						}
						else
						{
							input2.classList.remove('greenBg');
							input2.classList.add('redBg');
						}
					});

				}
			}
			if (options.inputs[i].type == 'hidden')
			{
				div.appendChild(input);
			}
			else
			{
				labelColumn.innerHTML = options.inputs[i].label;
				row.appendChild(fieldColumn);
				row.appendChild(labelColumn);
				tbody.appendChild(row);
			}
		}
		table.appendChild(tbody);
		div.appendChild(table)
	}
	div.appendChild(document.createElement('hr'));

	var cancel = document.createElement('input');
	cancel.type = 'button';
	cancel.value = "Cancel";
	cancel.addEventListener('click', function(){hideModal(options.id)});
	div.appendChild(cancel);

	if (options.buttons)
	{
		for (var i = 0; i < options.buttons.length; i++)
		{
			var button = document.createElement('input');
			button.type = 'button';
			button.value = options.buttons[i].value;
			button.addEventListener('click', options.buttons[i].event, true);
			if (options.buttons[i].type == 'close') button.addEventListener('click', function(){hideModal(options.id)});
			div.appendChild(button);
		}
	}

	document.body.appendChild(div);


	//прогрузка данных в выпадающие списки.
	//данный блок находится в конце функции, т.к. на момент инициализации элементов управления
	//они еще не загружены в документ, что не позволяет создавать для них дочерние узлы (данные)
	for (var i = 0; i < options.inputs.length; i++)
	{
		if (options.inputs[i].sp)
		{
			bullet.clear();
			bullet.sp = options.inputs[i].sp;
			bullet.pp = options.inputs[i].pp;
			bullet.initiator = options.inputs[i].id;
			
			if (options.inputs[i].args) bullet.args = options.inputs[i].args;

			bullet.send();
		}
	}

}

function fillComboBox(data, initiator)
{
	var cbx = document.getElementById(initiator);

	for(var i in data)
	{
		var option = document.createElement('option');
		if (data[i].ID) option.value = data[i].ID; //ID  заноситсЯ в value элемента
		if (data[i].id) option.value = data[i].id; //ID  заноситсЯ в value элемента

		for(var j in data[i])
		{
			if((j != 'ID')&&(j != 'id'))
			{
				option.appendChild(document.createTextNode(data[i][j]));
				break;
			}
		}
		cbx.appendChild(option);
	}

	// дожидаемся загрузки данных на форму и ставим в выпадающий список предустановленное значение
	var timerId = setInterval(function() {

		if (!document.getElementById('ID')) clearInterval(timerId)

		else {
			if (document.getElementById('ID').value != '')
			{
				if(document.getElementById('ID').value == '0')
				{
					// с ID==0 это новый элемент
				}
				else
				{
					cbx.value = document.getElementById(initiator+'ID').value;
				}
				clearInterval(timerId);
			}
		}


	}, 100);

}

//отображение модального окна
function showModal(divmodal)
{
	modal = document.getElementById(divmodal);
	shade = document.getElementById("shade");

	//создаем тень и блокируем дисплей
	shade.style.filter = "alpha(opacity=80)";
	shade.style.opacity = 0.5;
	shade.style.display = "block";

	// 3адаем блокироваку и отступ модального окна
	modal.style.display = "block";
	modal.style.top = "50px";
	modal.style.left = "50px";
}

//3aкрытие модального окна
function hideModal(divmodal)
{
	document.getElementById(divmodal).remove();
	document.getElementById("shade").remove();
}


// МОДАЛЬНЫЕ ОКНА
//================================================================
// ДИАЛОГОВЫЕ ОКНА

function TabForm(fmName)
{
	this.caption = fmName;
	this.id = fmName;
	
	//this.inputs = [];

	this.tabs = [];
	this.content = [];
	this.buttons = [];
	
	this.addTab = function(tabName)
	{
		var tab = 
		{
			caption: tabName
		}
		this.tabs.push(tab);
	}
	
	this.addTextField = function(inputID, inputLabel, defaulValue, multiLang)
	{
		this.content.push(
		{
			type: 'text',
			id: inputID,
			label: inputLabel,
			value: defaulValue,
			ml: multiLang
		});
	} 
	
	
	this.compile = function(parent)
	{
		//parent может быть объектом, либо ИД объекта
		if (typeof(parent)=='string') parent = document.getElementById(parent);	
		
		var tabs = document.createElement('div');
		tabs.classList.add('tabs-header');
		tabs.classList.add('tabs');
		
	
		// формирование закладок
		for (var i=0; i<this.tabs.length; i++)
		{	
			var radio = document.createElement('input');
			radio.type = 'radio';
			if (i==0) radio.checked = 1;
			radio.name = 'tabs';
			radio.lang = this.tabs[i].caption;
			radio.id = 'tab'+(i+1).toString();
			radio.addEventListener('change', function(){tabHandler(this);});
			tabs.appendChild(radio);	
			
			var label = document.createElement('label');
			label.htmlFor = radio.id;
			label.innerHTML = this.tabs[i].caption;
			var cap = this.tabs[i].caption;
			tabs.appendChild(label);
		}
		if (this.tabs.length>0) parent.appendChild(tabs);
		
		var frm = document.createElement('div');
		frm.classList.add('tab-content');
		
		var header = document.createElement('h2');
		header.appendChild(document.createTextNode(this.caption));
		frm.appendChild(header);
		
		if (this.content)
		{
			for (var i=0; i<this.content.length; i++)
			{
				if (this.content[i].type == 'select')
				{
					var div = document.createElement('div');
					
					//создание скрытого поля для хранения значения ИД выпадающего списка - для выбора значения по умолчанию
					var hiddenID = document.createElement('input');
					hiddenID.type = 'hidden';
					hiddenID.id =  this.content[i].id+'ID';

					div.appendChild(hiddenID);

					var select = document.createElement('select');
					if(this.content[i].mult) select.multiple="multiple";
					select.id = this.content[i].id;
					if (this.content[i].event) select.addEventListener('change', this.content[i].event);		
					div.appendChild(select);
					
					var label = document.createElement('label');
					label.htmlFor = select.id;
					label.appendChild(document.createTextNode(this.content[i].label));
					div.appendChild(label);
					
					frm.appendChild(div);
				}
				else if (this.content[i].type == 'textBlock')
				{
					//оставляем задел под текстовые блоки
				}
				else
				{
					if (this.content[i].ml) 
					{
						for (j=0; j<this.tabs.length; j++)
						{	
							var div = document.createElement('div');
							div.lang = this.tabs[j].caption;
							
							var input = document.createElement('input');
							input.id =  this.content[i].id+'_'+this.tabs[j].caption;
							input.type = this.content[i].type;
							if (this.content[i].value)
							{
								if (input.type == 'date') {input.value = dateForInput(this.content[i].value);}
								else {input.value = this.content[i].value;}
							}
							div.appendChild(input);
							
							var label = document.createElement('label');
							label.htmlFor = input.id;
							label.appendChild(document.createTextNode(this.content[i].label));
							div.appendChild(label);
							
							frm.appendChild(div);	
						}
					}
					else
					{
						var div = document.createElement('div');
						
						var input = document.createElement('input');
						input.id = this.content[i].id;
						input.type = this.content[i].type;
						if (this.content[i].value)
						{
							if (input.type == 'date') {input.value = dateForInput(this.content[i].value);}
							else {input.value = this.content[i].value;}
						}
						div.appendChild(input);
						
						var label = document.createElement('label');
						label.htmlFor = input.id;
						label.appendChild(document.createTextNode(this.content[i].label));
						div.appendChild(label);
						
						frm.appendChild(div);	
					}
				}
				
				if (this.content[i].type == 'password')	//для полей типа password навешивается обработчик событий, который проверяет соответствие пароля и его повтора
				{
					var input2;
					
					if (this.content[i].id.substr(0,4)=='rep-')
					{
						input.addEventListener('keyup', function()
						{
							var input2 = document.getElementById(this.id.substring(4));
							if (this.value == input2.value)
							{
								this.classList.remove('redBg');
								this.classList.add('greenBg');
							}
							else
							{
								this.classList.remove('greenBg');
								this.classList.add('redBg');
							}
						});
					}
					else
					{
						input.addEventListener('keyup', function()
						{
							var input2 = document.getElementById('rep-'+this.id);
							if (this.value == input2.value)
							{
								input2.classList.remove('redBg');
								input2.classList.add('greenBg');
							}
							else
							{
								input2.classList.remove('greenBg');
								input2.classList.add('redBg');
							}
						});

					}
				}
				
			}
		}
		
		frm.appendChild(document.createElement('hr'));
		
		var cancel = document.createElement('input');
		cancel.type = 'button';
		cancel.value = "Cancel";
		cancel.addEventListener('click', function() {document.getElementsByTagName('dialog')[0].close( );});
		
		frm.appendChild(cancel);
		
		
		
		if (this.buttons)
		{
			for (var i = 0; i < this.buttons.length; i++)
			{
				var button = document.createElement('input');
				button.type = 'button';
				button.value = this.buttons[i].value;
				button.addEventListener('click', this.buttons[i].event, true);
				if (this.buttons[i].type == 'close') button.addEventListener('click', function(){document.getElementsByTagName('dialog')[0].close( );});
				frm.appendChild(button);
			}
		}
		
		parent.appendChild(frm);
		var firstTab = document.querySelector('.tab-content div'); //фильтруются поля на первой закладке
		tabHandler(firstTab);
	}
	
}
//================================ПРОВЕРЕНО===============================

function tabHandler(obj)
{
	var fields = document.querySelector('.tab-content').getElementsByTagName('div');
	
	for (var i=0; i<fields.length; i++)
	{
		if ((fields[i].lang==obj.lang)||(fields[i].lang=='')) fields[i].classList.remove('div-hidden');
		else fields[i].classList.add('div-hidden');
	}
}


function DialogWindow()
{
	var dialog = document.createElement('dialog');
	document.getElementsByTagName('BODY')[0].appendChild(dialog);
	
	dialog.addEventListener('close', function() 
	{
		this.remove();
	});
	
	return dialog;
}

// ДИАЛОГОВЫЕ ОКНА
//================================================================
// ФОРМА ПОДРОБНОЙ ИНФОРМАЦИИ

function buildDetailsForm(parentDiv, element)	//"DivRightColumn", element
{
	var div = document.getElementById(parentDiv);
	while(div.firstChild) div.removeChild(div.firstChild);

	var table = document.createElement('table');
	table.className='hiddenBorders';
	var tbody = document.createElement('tbody');

	for (var key in element)
	{
		var row = document.createElement('tr');

		var fieldColumn = document.createElement('td');
		var labelColumn = document.createElement('td');

		/*
		var input = document.createElement('input');
		input.type = "text";
		input.id =  key;
		input.value = element[key];

		fieldColumn.appendChild(input);
		*/

		fieldColumn.innerHTML = element[key];
		labelColumn.innerHTML = key;
		row.appendChild(labelColumn);
		row.appendChild(fieldColumn);
		tbody.appendChild(row);

	}

	table.appendChild(tbody);
	div.appendChild(table);
	div.appendChild(document.createElement('hr'));
}

function buildStructuredDetailsForm(parentDiv, data)
{

	function sortByOrder(a, b) //метод сортировки элементов дерева с учетом их принадлежности к ветке
	{
		if (a.order > b.order) return 1;
		if (a.order < b.order) return -1;
	}

	data.sort(sortByOrder);

	var div = document.getElementById(parentDiv);
	while(div.firstChild) div.removeChild(div.firstChild);

	var table = document.createElement('table');
	table.className='hiddenBorders';
	var tbody = document.createElement('tbody');

	for (var i=0; i<data.length; i++)
	{
		var row = document.createElement('tr');

		var fieldColumn = document.createElement('td');
		var labelColumn = document.createElement('td');



		fieldColumn.innerHTML = data[i].value;
		labelColumn.innerHTML = data[i].propName;
		row.appendChild(labelColumn);
		row.appendChild(fieldColumn);
		tbody.appendChild(row);

	}

	table.appendChild(tbody);
	div.appendChild(table);
	div.classList.add('brd');
}


// ФОРМА ПОДРОБНОЙ ИНФОРМАЦИИ
//================================================================
// ПОСТРОЕНИЕ ТАБЛИЦ

// Функция, формирующая таблицу
// arr - массив данных, на основе которого строится таблица при этом:
//			элементы массива - объекты, являющиеся строками таблицы, у которых свойства-значение поля таблицы
//			заголовки таблицы берутся из имен свойств первого элемента массива - объекта arr[0]
// tabID - id, присваиваемое DOM-объекту <TABLE>
// parent - id DOM-объекта, внутри которого будет построена таблица
function buildTable(arr, tabID, parent)
{
	if (arr.length == 0) return;
	
	for (var key in arr[0])
	{
		if (typeof(arr[0][key])!=='object') arr = adoptArr(arr);
		break;
	}
	//else 
	
	
	var div = document.getElementById(parent)
	var table = document.createElement('table');
	table.id = tabID;
	var thead = document.createElement('thead');
	thead.className='tableHead';

	// создание заголовка
	var headRow = document.createElement('tr');
	for (var key in arr[0]){
		if (arr[0][key].visible != 0)
		{
			var td = document.createElement('td');
			td.innerHTML = key;
			headRow.appendChild(td);
		}
	}
	thead.appendChild(headRow);
	table.appendChild(thead);

	var tbody = document.createElement('tbody');
	tbody.addEventListener("click", highlightRow, true);
	tbody.classList.add('touchable');

	for (var i=0; i< arr.length; i++)
	{
		var tr = document.createElement('tr');
		for (var key in arr[i])
		{
			if (arr[i][key].visible == 0)
			{
				tr.dataset[key] = arr[i][key].value; //назначаем ID строке
			}
			else
			{
				var td = document.createElement('td');
				td.innerHTML = arr[i][key].value;
				tr.appendChild(td);
			}
			if (key.toLowerCase() == 'id')
			{
				tr.dataset.id = arr[i][key].value; //назначаем ID строке
			}
		/*
			else
			{
				var td = document.createElement('td');
				td.innerHTML = arr[i][key].value;
				tr.appendChild(td);
			}
		*/
		}
		tbody.appendChild(tr);
	}
	table.appendChild(tbody);
	div.appendChild(table);
}

function adoptArr(arr)
{
	var tabl = new Array();

	for (var i=0; i<arr.length; i++)
	{
		tabl[i] = new Object();
		
		for (var key in arr[i])
		{
			tabl[i][key] = {value: arr[i][key], visible: 1}
		}
	}
	return tabl;
}


function highlightRow(event)  //handler-фунци¤ дл¤ подсветки строк таблицы
{
	var target = event.target;
	while (target.tagName !=  'TABLE') // цикл движетс¤ вверх от target к родител¤м до table
	{
		if (target.tagName == 'TR')  // нашли элемент, который нас интересует!
		{
			highlight(target);
			target.parentNode.parentNode.dataset.selectedId = target.dataset.id;
			return;
		}
		target = target.parentNode;
	}
}

function highlight(node) 	// перекрашивание выделенной строки в таблице
{
	var selectedRows = node.parentNode.getElementsByClassName('highlight');
	if (selectedRows.length > 0) selectedRows[0].classList.remove('highlight');
	node.classList.add('highlight');
}

// ПОСТРОЕНИЕ ТАБЛИЦ
//================================================================
// КОНТЕКСТНОЕ МЕНЮ

function linkContextMenu(parent, menu){
	var elem = document.getElementById(parent);

	elem.oncontextmenu = function(event)
	{
		event.preventDefault();
		var cMenu = document.createElement('div');
		cMenu.classList.add('contx');

		for (var key in menu)
		{
			var menuElem = document.createElement('div');
			menuElem.innerHTML = key;
			menuElem.classList.add('cMenuElem');
			menuElem.addEventListener("click", function(){this.parentNode.remove();});
			menuElem.addEventListener("click", menu[key]);
			cMenu.appendChild(menuElem);
		}
		cMenu.onmouseout = function(event)	//менюшный див убиваетс¤ при выведении курсора за его границы
		{
			if ((event.pageX > this.offsetLeft) && (event.pageX < this.offsetLeft+this.offsetWidth)&&(event.pageY > this.offsetTop) && (event.pageY < this.offsetTop+this.offsetHeight)) return;
			this.remove();
		}
		cMenu.style.left = (event.pageX-15)+'px';
		cMenu.style.top = (event.pageY-5)+'px';
		document.body.appendChild(cMenu);

	}
}

//alert("components loaded");

// КОНТЕКСТНОЕ МЕНЮ
//================================================================
