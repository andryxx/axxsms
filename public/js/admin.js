document.addEventListener('DOMContentLoaded', start);

function start()
{
	var metas = document.getElementsByTagName('meta');
	for (var i=0; i< metas.length; i++){
		if (metas[i].hasAttribute('sessionid'))
		{
			localStorage.setItem('axxSessionID', metas[i].getAttribute('sessionid'));
			metas[i].remove();
		}
		if (metas[i].hasAttribute('role'))
		{
			localStorage.setItem('axxRole', metas[i].getAttribute('role'));
			metas[i].remove();
		}
	}
	
	genMainMenu();
	vauLnkClick();
}

function genMainMenu()
{

	var mainMenu = document.getElementById('mainMenu');
	while(mainMenu.firstChild) mainMenu.removeChild(mainMenu.firstChild);	
	
	var sysRole = localStorage.getItem('axxRole');
	if (sysRole <= 1)
	{
		var vauLnk = document.createElement('a');
		vauLnk.href = '#';
		vauLnk.appendChild(document.createTextNode('Vauchers'));
		vauLnk.addEventListener('click', vauLnkClick);
		mainMenu.appendChild(vauLnk);
		
		var netwLnk = document.createElement('a');
		netwLnk.href = '#';
		netwLnk.appendChild(document.createTextNode('Networх'));
		netwLnk.addEventListener('click', netwLnkClick);
		mainMenu.appendChild(netwLnk);
		
		var usrsLnk = document.createElement('a');
		usrsLnk.href = '#';
		usrsLnk.appendChild(document.createTextNode('Users'));
		usrsLnk.addEventListener('click', usrsLnkClick);
		mainMenu.appendChild(usrsLnk);
	}
	
	if (sysRole == 0)
	{
		var acMxLnk = document.createElement('a');
		acMxLnk.href = '#';
		acMxLnk.appendChild(document.createTextNode('Access Matrix'));
		acMxLnk.addEventListener('click', accMxClick);
		mainMenu.appendChild(acMxLnk);	
	}


	
}

function clearContent()
{
	document.getElementById('tools').innerHTML = '';
	document.getElementById('leftColumn').innerHTML = '';
	document.getElementById('rightColumn').innerHTML = '';
}

function vauLnkClick()
{
	clearContent();
	
	//var tools = document.getElementById('tools');
	//while(tools.firstChild) tools.removeChild(tools.firstChild);
	
	var search = document.createElement('input');
	search.type = 'text';
	search.id = 'searchField';
	search.placeholder='Search';
	search.classList.add('toolItem');
	search.addEventListener('keyup', loadCredetialsTable)
	tools.appendChild(search);
	
	var cred = document.createElement('button');
	cred.innerHTML = '+ Creds';
	cred.classList.add('toolItem');
	cred.addEventListener('click', addCredentials);
	tools.appendChild(cred);
	
	var vaucher = document.createElement('button');
	vaucher.innerHTML = '+ Vaucher';
	vaucher.classList.add('toolItem');
	vaucher.id = 'btnVaucher';
	vaucher.setAttribute('hidden', '');
	vaucher.addEventListener('click', genVaucher);
	tools.appendChild(vaucher);
	
	var lbl = document.createElement('label');
	lbl.classList.add('toolItem');
	lbl.id = 'lblVaucher';
	tools.appendChild(lbl);
	
	loadCredetialsTable();
}

function usrsLnkClick()
{
	clearContent();
	genSysUsersTools();
	loadUsersTable();
	
}

function netwLnkClick()
{
	clearContent();
	genNetworksTools();
	loadNetworksTable();
}

function loadCredetialsTable()
{
	var div = document.getElementById("rightColumn");
	while(div.firstChild) div.removeChild(div.firstChild);
	
	filter = document.getElementById('searchField').value;
	
	bullet.clear();
	bullet.sp = 'ShowCredList';
	bullet.pp = 'fillCredetialsTable'
	
	bullet.args.filter = filter;	
	bullet.args.limit = '0';	
	bullet.args.amount = '10';	
	
	bullet.send();
}

function fillCredetialsTable(data)
{
	document.getElementById('lblVaucher').innerHTML = '';
	document.getElementById('btnVaucher').setAttribute('hidden', '');
	
	var myDiv   = document.getElementById('leftColumn');
	while(myDiv.firstChild) myDiv.removeChild(myDiv.firstChild);
	var tabl = new Array();

	for (var i=0; i<data.length; i++)
	{
		tabl[i] = new Object();

		tabl[i].id = {value: data[i].id, visible: 0};
		tabl[i].lastName = {value: data[i].lastName, visible: 1};
		tabl[i].firstName = {value: data[i].firstName, visible: 1};
		tabl[i].middleName = {value: data[i].middleName, visible: 1};
		tabl[i].country = {value: data[i].country, visible: 1};
		tabl[i].passport = {value: data[i].passport, visible: 1};
		tabl[i].created = {value: data[i].when, visible: 1};
	}

	if (data.length>0) buildTable(tabl, 'tabCredentials', 'leftColumn');

	
	var tabl = document.getElementById('tabCredentials');
	if (tabl)
	{
		var body = tabl.getElementsByTagName('TBODY');
		body[0].addEventListener("click", function()
		{
			document.getElementById('btnVaucher').removeAttribute('hidden');
			/*
			var div = document.getElementById("rightColumn");
			while(div.firstChild) div.removeChild(div.firstChild);
			*/
			
			loadVauchersTable();
			
			var elements = body[0].getElementsByTagName('tr');
			for (var i=0; i<elements.length; i++)
			{
				if (elements[i].dataset.id == tabl.dataset.selectedId)
				{
					//dirType = elements[i].dataset.hierarchy;
					
					
					document.getElementById('lblVaucher').innerHTML = 'for '+elements[i].firstChild.innerHTML+' '+elements[i].firstChild.nextSibling.innerHTML;
					
					break;
				}
			};
		
		});
	}
	
}

function addCredentials()
{
	var newCred = new ModalForm('New credentials');
	newCred.addTextField('lastName', 'Last Name', '');
	newCred.addTextField('firstName', 'First Name', '');
	newCred.addTextField('middleName', 'Middle Name', '');
	newCred.addTextField('country', 'Country', '');
	newCred.addTextField('passport', 'Passport ID', '');
	newCred.addTextField('phone', 'Phone', '');
	newCred.addCloseButton('OK', saveCredentials)
		
	newCred.show();
}

function saveCredentials()
{
	document.getElementById('lblVaucher').innerHTML = '';
	
	bullet.clear();
	bullet.sp = 'AddCredentials';
	bullet.pp = 'loadCredetialsTable';
	
	bullet.args.firstName = document.getElementById('firstName').value;
	bullet.args.middleName = document.getElementById('middleName').value;
	bullet.args.lastName = document.getElementById('lastName').value;
	bullet.args.country = document.getElementById('country').value;
	bullet.args.passport = document.getElementById('passport').value;
	bullet.args.phone = document.getElementById('phone').value;
	
	bullet.send();
}

function genVaucher()
{
	var vau = Math.round(Math.random()*100000000);
	localStorage.setItem('vaucher', vau);
	checkVaucher(vau, 'addVaucher')
}


function checkVaucher(vaucher, postProcessor)
{
	bullet.clear();
	bullet.sp = 'CheckVaucher';
	bullet.pp = postProcessor;
	bullet.result = 'single';
	
	bullet.args.vaucher = vaucher;
	
	bullet.send();
}

function addVaucher(checkResult)
{
	if (checkResult != 'none')
	{
		genVaucher();
	}
	else
	{
		//alert('checkResult:'+checkResult);
		bullet.clear();
		bullet.sp = 'AddVaucher';
		bullet.pp = 'loadVauchersTable';
		bullet.result = 'single';
		
		bullet.args.vaucher = localStorage.getItem('vaucher');
		bullet.args.credId = document.getElementById('tabCredentials').dataset.selectedId;
		
		//alert('bullet: '+JSON.stringify(bullet));
		
		bullet.send();
	}
}

function loadVauchersTable()
{
	var div = document.getElementById("rightColumn");
	while(div.firstChild) div.removeChild(div.firstChild);

	bullet.clear();
	bullet.sp = 'GetVauchersList';
	bullet.pp = 'fillVauchersTable';
	
	bullet.args.credId = document.getElementById('tabCredentials').dataset.selectedId;
	
	
	bullet.send();
	
}

function fillVauchersTable(data)
{
	var myDiv = document.getElementById('rightColumn');
	
	var tabl = new Array();

	for (var i=0; i<data.length; i++)
	{
		tabl[i] = new Object();

		tabl[i].id = {value: data[i].id, visible: 0};
		tabl[i].when = {value: data[i].when, visible: 0};
		tabl[i].vaucher = {value: data[i].vaucher, visible: 1};
		tabl[i].mac = {value: data[i].mac, visible: 1};
		
		var act = 'yes';
		if (data[i].activated == '0') act = 'no';
		
		tabl[i].activated = {value: act, visible: 1};
	}

	if (data.length>0) buildTable(tabl, 'tabVauchers', 'rightColumn');
	
	markLinks();
	
}

function markLinks()
{
	var tabl = document.getElementById('tabVauchers');
	if (tabl)
	{
		var body = tabl.getElementsByTagName('TBODY');
		var elements = body[0].getElementsByTagName('tr');
		for (var i=0; i<elements.length; i++)
		{
			if (elements[i].lastChild.innerHTML == 'no')
			{
				//alert(elements[i].lastChild.innerHTML);
				var ltext = elements[i].firstChild.innerHTML;
				var link = document.createElement('a');
				link.target = "_blank";
				link.href = "/print?code="+elements[i].firstChild.innerHTML;
				link.innerHTML = 'Print vaucher';
				elements[i].firstChild.nextElementSibling.innerHTML='';
				elements[i].firstChild.nextElementSibling.appendChild(link);
			}
		};
	}

}

function genNetworksTools()
{
	var add = document.createElement('button');
	add.innerHTML = 'Add';
	add.classList.add('toolItem');
	add.addEventListener('click', addNetwork);
	tools.appendChild(add);
	
	var edit = document.createElement('button');
	edit.innerHTML = 'Edit';
	edit.classList.add('toolItem');
	edit.setAttribute('hidden', '');
	edit.addEventListener('click', editNetwork);
	tools.appendChild(edit);	
	
	var del = document.createElement('button');
	del.innerHTML = 'Delete';
	del.classList.add('toolItem');
	del.setAttribute('hidden', '');
	del.addEventListener('click', delNetwork);
	tools.appendChild(del);

	var tst = document.createElement('button');
	tst.innerHTML = 'Test';
	tst.classList.add('toolItem');
	tst.addEventListener('click', tstFn);
	tools.appendChild(tst);
}




function loadNetworksTable()
{
	bullet.clear();
	bullet.sp = 'GetNetworks';
	bullet.pp = 'fillNetworkssTable'
		
	bullet.send();
}

function fillNetworkssTable(data)
{
	var tabl = new Array();

	for (var i=0; i<data.length; i++)
	{
		tabl[i] = new Object();

		tabl[i].id = {value: data[i].id, visible: 0};
		tabl[i].address = {value: data[i].address, visible: 1};
		tabl[i].bitmask = {value: data[i].bitmask, visible: 1};
		tabl[i].netmask = {value: data[i].netmask, visible: 1};
		tabl[i].wildcard = {value: data[i].wildcard, visible: 1};
		tabl[i].network = {value: data[i].network, visible: 1};
		tabl[i].hostMin = {value: data[i].hostMin, visible: 1};
		tabl[i].hostMax = {value: data[i].hostMax, visible: 1};
		tabl[i].broadcast = {value: data[i].broadcast, visible: 1};
		tabl[i].hosts = {value: data[i].hosts, visible: 1};
		tabl[i].min = {value: data[i].min, visible: 1};
		tabl[i].max = {value: data[i].max, visible: 1};
	}

	if (data.length>0) buildTable(tabl, 'tabNetworks', 'leftColumn');

	
	var tabl = document.getElementById('tabNetworks');
	if (tabl)
	{
		var body = tabl.getElementsByTagName('TBODY');
		body[0].addEventListener("click", function()
		{
			var elements = document.querySelectorAll('.toolItem');

			for (var i = 0; i < elements.length; i++) 
			{
				elements[i].removeAttribute('hidden');
			}
		});
	}
}

function addNetwork()
{
	createNetworkForm(true);
}

function createNetworkForm(isNew)
{
	var newNetwork = new ModalForm('New Network');
	
	newNetwork.addTextField('address', 'Address', '');
	newNetwork.addTextField('bitmask', 'Bitmask', '');
	newNetwork.addTextField('netmask', 'Netmask', '');
	newNetwork.addTextField('wildcard', 'Wildcard', '');
	newNetwork.addTextField('network', 'Network', '');
	newNetwork.addTextField('hostMin', 'Host min', '');
	newNetwork.addTextField('hostMax', 'Host max', '');
	newNetwork.addTextField('broadcast', 'Broadcast', '');
	newNetwork.addTextField('hosts', 'Hosts', '');

	if (isNew==true) newNetwork.addCloseButton('OK', saveNewNetwork)
	else newNetwork.addCloseButton('OK', saveNetworkChanges);
		
	newNetwork.show();
}

function saveNewNetwork()
{
	bullet.clear();
	bullet.sp = 'AddNetwork';
	bullet.pp = 'netwLnkClick';
	
	bullet.args.address = document.getElementById('address').value;
	bullet.args.bitmask = document.getElementById('bitmask').value;
	bullet.args.netmask = document.getElementById('netmask').value;
	bullet.args.wildcard = document.getElementById('wildcard').value;
	bullet.args.network = document.getElementById('network').value;
	bullet.args.hostMin = document.getElementById('hostMin').value;
	bullet.args.hostMax = document.getElementById('hostMax').value;
	bullet.args.broadcast = document.getElementById('broadcast').value;
	bullet.args.hosts = document.getElementById('hosts').value;
	bullet.args.min = ipToDecimal(bullet.args.hostMin);
	bullet.args.max = ipToDecimal(bullet.args.hostMax);
	
	bullet.send();
}

function ipToDecimal(ip)
{
	var arr = ip.split('.');
	return (arr[0]*256*256*256)+(arr[1]*256*256)+(arr[2]*256)+arr[3]*1;
}

function delNetwork()
{

	if (confirm("Are you sure you want to delete selected Network?")) 
	{
		bullet.clear();
		bullet.sp = 'DeleteNetwork';
		bullet.pp = 'netwLnkClick';
		
		bullet.args.id = document.getElementById('tabNetworks').dataset.selectedId;
	
	
		bullet.send();
	} 

}

function editNetwork()
{
	createNetworkForm(false);
	getNetworkDetails();
}

function getNetworkDetails()
{
	bullet.clear();
	bullet.sp = 'GetNetworkDetails';
	bullet.pp = 'fillNetworkForm';
	
	bullet.args.id = document.getElementById('tabNetworks').dataset.selectedId;

	bullet.send();
}


function fillNetworkForm(data)
{
	document.getElementById('address').value = data[0].address;
	document.getElementById('bitmask').value = data[0].bitmask;
	document.getElementById('netmask').value = data[0].netmask;
	document.getElementById('wildcard').value = data[0].wildcard;
	document.getElementById('network').value = data[0].network;
	document.getElementById('hostMin').value = data[0].hostMin;
	document.getElementById('hostMax').value = data[0].hostMax;
	document.getElementById('broadcast').value = data[0].broadcast;
	document.getElementById('hosts').value = data[0].hosts;
}

function saveNetworkChanges()
{
	//alert('Увы! система еще не доделана до конца :( Приходите в следующий раз.');
	
	bullet.clear();
	bullet.sp = 'ChangeNetwork';
	bullet.pp = 'netwLnkClick';
	
	bullet.args.id = document.getElementById('tabNetworks').dataset.selectedId;
	bullet.args.address = document.getElementById('address').value;
	bullet.args.bitmask = document.getElementById('bitmask').value;
	bullet.args.netmask = document.getElementById('netmask').value;
	bullet.args.wildcard = document.getElementById('wildcard').value;
	bullet.args.network = document.getElementById('network').value;
	bullet.args.hostMin = document.getElementById('hostMin').value;
	bullet.args.hostMax = document.getElementById('hostMax').value;
	bullet.args.broadcast = document.getElementById('broadcast').value;
	bullet.args.hosts = document.getElementById('hosts').value;
	bullet.args.min = ipToDecimal(bullet.args.hostMin);
	bullet.args.max = ipToDecimal(bullet.args.hostMax);
	
	bullet.send();
}

function tstFn()
{
/*
	alert(localStorage.getItem('axxSessionID'));
	alert(localStorage.getItem('axxRole'));

	bullet.clear();
	//bullet.result = 'credentials';
	bullet.sp = 'GetSessions2';
	bullet.pp = 'noFunc';
	
	//bullet.args.sessionId = localStorage.getItem('axxSessionID');
	
	bullet.send();
*/	
}

function accMxClick()
{

}

function genSysUsersTools()
{
	var add = document.createElement('button');
	add.innerHTML = 'Add';
	add.classList.add('toolItem');
	add.addEventListener('click', addSysUser);
	tools.appendChild(add);
	
	var edit = document.createElement('button');
	edit.innerHTML = 'Edit';
	edit.classList.add('toolItem');
	edit.setAttribute('hidden', '');
	edit.addEventListener('click', editSysUser);
	tools.appendChild(edit);	
	
	var del = document.createElement('button');
	del.innerHTML = 'Delete';
	del.classList.add('toolItem');
	del.setAttribute('hidden', '');
	del.addEventListener('click', delSysUser);
	tools.appendChild(del);
}

function loadUsersTable()
{
	bullet.clear();
	bullet.sp = 'GetSysUsersList';
	bullet.pp = 'fillUsersTable';

	bullet.send();
}

function fillUsersTable(data)
{
	var tabl = new Array();

	for (var i=0; i<data.length; i++)
	{
		tabl[i] = new Object();

		tabl[i].id = {value: data[i].id, visible: 0};
		tabl[i].username = {value: data[i].username, visible: 1};
		tabl[i].description = {value: data[i].description, visible: 1};
		tabl[i].role = {value: data[i].role, visible: 1};
		tabl[i].email = {value: data[i].email, visible: 1};
	}

	if (data.length>0) buildTable(tabl, 'tabSysUsers', 'leftColumn');

	
	var tabl = document.getElementById('tabSysUsers');
	if (tabl)
	{
		var body = tabl.getElementsByTagName('TBODY');
		body[0].addEventListener("click", function()
		{
			//обработчик клика по строке таблицы
			var elements = document.querySelectorAll('.toolItem');

			for (var i = 0; i < elements.length; i++) 
			{
				elements[i].removeAttribute('hidden');
			}
		});
	}
}

function addSysUser()
{
	createUserModalForm(true);
}


function createUserModalForm(isNew)
{
	var newUsrForm = new ModalForm('New System User');
	
	newUsrForm.addTextField('username', 'Username', '');
	newUsrForm.addPasswordField('password', 'Password', '');
	newUsrForm.addTextField('description', 'Description', '');
	
	var dataSource =
	{
		storedProcedure: 'GetSysRolesList',
		postporcessor: 'addRolesInModalCBox',
		arguments: {}
	};
	
	newUsrForm.addComboBox('cbx_role', 'Role', dataSource);
	newUsrForm.addTextField('email', 'Email', '');

	
	if (isNew==true) newUsrForm.addCloseButton('OK', function() {SaveSysUser(true)})
	else newUsrForm.addCloseButton('OK', function() {SaveSysUser(false)});
	
	
	newUsrForm.show();
}


function addRolesInModalCBox(data)
{
	var cbx = document.getElementById('cbx_role');
	for(var i in data)
	{
		var option = document.createElement('option');
		option.value = data[i].id; 
		option.appendChild(document.createTextNode(data[i].description));
		cbx.appendChild(option);
	}
}

function SaveSysUser(isNew)
{

	bullet.clear();
	if (isNew) bullet.sp = 'AddSysUser';
	else bullet.sp = 'EditSysUser';
	bullet.pp = 'usrsLnkClick';

	bullet.args.username = document.getElementById('username').value;
	bullet.args.password = document.getElementById('password').value;
	bullet.args.description = document.getElementById('description').value;
	bullet.args.role = document.getElementById('cbx_role').value;
	bullet.args.email = document.getElementById('email').value;
	

	bullet.send();
}


function editSysUser()
{
	createUserModalForm(false);
	loadSysUserDetails();
}

function loadSysUserDetails()
{

	bullet.clear();
	bullet.sp = 'GetSysUserDetails';
	bullet.pp = 'fillUserModalForm';
	bullet.args.eid = document.getElementById('tabSysUsers').dataset.selectedId;

	bullet.send();

}

function fillUserModalForm(data)
{
	var sysUser = data[0];
	document.getElementById('username').value = sysUser.username;
	document.getElementById('description').value = sysUser.description;
	document.getElementById('cbx_role').value = sysUser.role;
	document.getElementById('email').value = sysUser.email;
	
}

function delSysUser()
{
	bullet.clear();
	bullet.sp = 'DeleteSysUser';
	bullet.pp = 'usrsLnkClick';
	bullet.args.eid = document.getElementById('tabSysUsers').dataset.selectedId;

	bullet.send();
	//alert('delSysUser');

}